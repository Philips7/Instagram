import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom'

import logo from './logo.svg';
import './App.css';
import Header from "./shared/header/Header";
import Footer from "./shared/footer/Footer";
import PhotoGrid from "./features/components/instagram/PhotoGrid";
import SinglePhoto from "./features/components/instagram/SinglePhoto";


export class App extends Component {
    render() {
        return (
            <div>
                <Header/>
                    <Switch>
                        <Route path="/single" component={PhotoGrid}/>
                        <Route path="/" component={SinglePhoto}/>
                    </Switch>
                <Footer/>
            </div>
        );
    }
}

export default App;
