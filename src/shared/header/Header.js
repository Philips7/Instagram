import React, {Component} from 'react';
import {Link} from 'react-router-dom'


export class Header extends Component {
    render() {
        return (
            <div>
                <ul>
                    <li><Link to="/">Instagram</Link></li>
                    <li><Link to="/single">Single</Link></li>
                </ul>
                <h1>Header</h1>
            </div>
        );
    }
}

export default Header;
